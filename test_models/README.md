# Description of the tests

The tests are named sequentially, as `test_#.htc` according to the following list.

## Saturation angles applied, angular speed

1. Set actuation limit -40:40 and rad/s to 1 deg/s: request to the servo for an angle of -50 at 10 seconds. It should reach a -40 deg. Run a 60s simulation.
2. Set actuation limit -40:40 and rad/s to 1 deg/s: request to the servo for an angle of 50 at 10 seconds. It should give a +40 deg angle. Run a 60s simulation.

## Angular wrapping applied

3. Set actuation limit -90:90 and rad/s to 5 deg/s: request to the servo for an angle of 410 deg at 10 seconds. It should give a 50 deg angle at 20 s. Run a 30s simulation.
4. Set actuation limit -90:90 and rad/s to 5 deg/s: request to the servo for an angle of -410deg at 10 seconds. It should give a 50 deg angle at 20s. Run a 30s simulation.
5. Set actuation limit -360:360 and rad/s to 5 deg/s: request to the servo for an angle of 720deg at 8 seconds. It should give a 360 rotation at 80s. Run a 90s simulation.

## Blade stuck

6. Set actuation limit -90:90 and rad/s to 1 deg/s: request to the servo for an angle of 20deg at 10 seconds. Give an input of blade stuck to the controller at 15s and let it run for 30s. It should be stuck for at 5 deg for the last 5 sec.

## Pitch runaway

7. Set actuation limit -90:90 and rad/s to 1 deg/s: request to the servo for an angle of 20deg at 10 seconds. Give an input of pitch runaway at 20s. It should be stop increasing angle at 20 sec (about 10 deg) and then go to -90 at about 110s-115s.

## Stair signal

8. Stairs signal with 2 complete turns. The goal is to observe the frequency and damping, and see if it follows correctly.

## Saw tooth signal

9. The demanded angle prescribes a few turns clockwise and then a few more counterclockwise. Since the angular speed is constant, we expect oscillations only at the beginning and when the rotation is reversed. This is a test for the unwrap functionality. 

## Normal usage as pitch actuator

10. Use some existing HAWC2 test (?)
