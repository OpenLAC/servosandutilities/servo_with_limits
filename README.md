# servo_with_limits

[![pipeline status](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/servo_with_limits/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/servo_with_limits/-/commits/master)

DLL that implements a second order actuator with limits. It is used with HAWC2 to model a pitch bearing.

The documentation is available at: https://openlac.pages.windenergy.dtu.dk/servosandutilities/servo_with_limits
