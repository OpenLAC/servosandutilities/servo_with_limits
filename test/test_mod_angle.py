# -*- coding: utf-8 -*-
"""
Test mod_angle.

@author: ricriv
"""

import os.path
from ctypes import CDLL, POINTER, byref, c_double

import numpy as np
import pytest

name = "servo_with_limits"


@pytest.fixture()
def dll():
    # Load dll.
    path = f"build/{name}.so"  # Linux.
    if os.path.exists(path):
        return CDLL(path)
    path = f"build/{name}_64.dll"  # Win64.
    if os.path.exists(path):
        return CDLL(path)


@pytest.mark.skipif(os.path.exists(f"build32/{name}.dll"), reason="Skip Win32")
def test_unwrap_saw_tooth(dll):

    # Make test data.
    time = np.linspace(0.0, 100.0, 1001, dtype=c_double)
    a = np.zeros_like(time)
    mask = time < 50.0
    a[mask] = (2.0 * np.pi * 0.1 * time[mask]) % (2 * np.pi)
    a[~mask] = (2.0 * np.pi * (-0.1) * time[~mask]) % (2 * np.pi)
    a[500] = 2.0 * np.pi  # Fix angle at 50 seconds.
    a = a.astype(c_double, order="F")

    # Make reference results.
    res_ref = np.unwrap(a)

    # # Define dll signature.
    dll.unwrap.argtypes = [
        POINTER(c_double),  # x_old
        POINTER(c_double),  # x_new
        POINTER(c_double),  # x_unwrap
    ]
    dll.unwrap.restype = None

    # # Use dll.
    res_dll = np.full(a.shape, np.nan, dtype=c_double, order="F")
    res_dll[0] = a[0]
    val = c_double(0.0)
    for k in range(1, time.size):
        dll.unwrap(byref(c_double(res_dll[k - 1])), byref(c_double(a[k])), byref(val))
        res_dll[k] = val.value

    # Test.
    np.testing.assert_array_almost_equal_nulp(res_dll, res_ref)

    if False:
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots()
        ax.set_xlabel("Time [s]")
        ax.set_ylabel("Angle [rad]")
        ax.plot(time, a, label="Original")
        ax.plot(time, res_ref, label="Numpy")
        ax.plot(time, res_dll, ls="--", label="DLL")
        ax.legend()
