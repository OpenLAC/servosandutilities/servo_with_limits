# -*- coding: utf-8 -*-
"""
Write table with saw tooth for test_9.

@author: ricriv
"""

import numpy as np

time = np.array(
    [
        0.0,
        9.99,
        10.0,
        19.99,
        20.0,
        29.99,
        30.0,
        39.99,
        40.0,
        50.0,
        60.0,
        60.01,
        70.0,
        70.01,
        80.0,
        80.01,
        90.0,
        90.01,
        100.0,
    ]
)

a = np.zeros_like(time)
mask = time < 50.0
a[mask] = (2.0 * np.pi * 0.1 * time[mask]) % (2 * np.pi)
a[~mask] = (2.0 * np.pi * (-0.1) * time[~mask]) % (2 * np.pi)
a[9] = 2.0 * np.pi  # Fix angle at 50 seconds.

arr = np.column_stack((time, a, a, a))

np.savetxt(
    "../test_models/control/table_saw_tooth.txt",
    arr,
    fmt="%6.2f  %.8f  %.8f  %.8f",
    header=f"{arr.shape[0]} {arr.shape[1]}",
    comments="",
)

if False:
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()
    ax.plot(time, a)
