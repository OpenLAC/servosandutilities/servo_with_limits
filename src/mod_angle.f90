module mod_angles

! The subroutines contained in this module have no ambition to be general.
! They provide the functionalities needed in the context of this dll, and nothing more.

use, intrinsic :: iso_c_binding, only: c_int, c_double

implicit none

integer, parameter :: wp = c_double
    !! Working precision. HAWC2 needs double.
real(wp), parameter :: pi = 4.0_wp * atan(1.0_wp)
    !! pi.
real(wp), parameter :: two_pi = 2.0_wp * pi
    !! 2 * pi.
real(wp), parameter :: deg2rad = pi / 180.0_wp
    !! Conversion factor from degrees to radians.
real(wp), parameter :: rad2deg = 180.0_wp / pi
    !! Conversion factor from radians to degrees.

contains

real(wp) pure function f_unwrap(x_old, x_new) result(x_unwrap)

    !! Unwrap an angle to eliminate discontinuities.
    !! The computation is done online, which means that this function works with scalars.

real(wp), intent(in) :: x_old
    !! Value at the previous time step.
real(wp), intent(in) :: x_new
    !! Value at the current time step.

real(wp) :: dx

dx = x_new - x_old
x_unwrap = x_new - sign(two_pi, dx) * real(floor((abs(dx) + pi) / two_pi), wp)

end function f_unwrap


pure subroutine unwrap(x_old, x_new, x_unwrap) bind(C, name='unwrap')
!DEC$ IF .NOT. DEFINED(__LINUX__)
!DEC$ ATTRIBUTES DLLEXPORT :: unwrap
!DEC$ ENDIF

    !! Unwrap an angle to eliminate discontinuities.
    !! The computation is done online, which means that this function works with scalars.

real(wp), intent(in) :: x_old
    !! Value at the previous time step.
real(wp), intent(in) :: x_new
    !! Value at the current time step.
real(wp), intent(out) :: x_unwrap
    !! Value at the current time step after unwrap operation.

x_unwrap = f_unwrap(x_old, x_new)

end subroutine unwrap


end module mod_angles
