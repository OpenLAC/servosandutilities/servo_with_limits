---
project: Servo with limits dll
author: DTU Wind and Energy Systems
year: 2024
license: mit
doc_license: by
preprocess: False
project_gitlab: https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/servo_with_limits
output_dir: build_docs
page_dir: docs
media_dir: docs/fig
proc_internals: true
display: public
         protected
         private
---

This dll allows to model an actuator.
