; DTU 10MW RWT. https://gitlab.windenergy.dtu.dk/hawc-reference-models/dtu-10-mw
; 
begin simulation;
  time_stop    60.0;  total time to simulate
  log_deltat 50.0;
  solvertype   2 ;  1=dense newmark, 2= sparse newmark (12.7+)
  on_no_convergence continue ;
  convergence_limits 1E3 1.0 1E-7 ;
  logfile ./log/test_2.log ;
  begin newmark;
    deltat    0.01;  time step
  end newmark;
end simulation;
;
;-------------------------------------------------------------------------------------------------
continue_in_file  two_beams_with_bearing.htc ; 
;-------------------------------------------------------------------------------------------------
begin dll;  dlls used in model
;
  begin type2_dll;  4. pitch servo
    name servo_with_limits ;
    filename  ./control/servo_with_limits.dll ;
    dll_subroutine_init init_servo_with_limits ;
    dll_subroutine_update update_servo_with_limits ;
    arraysizes_init  100 1 ;
    arraysizes_update  5 3 ;
	begin init ;
      constant 1   1    ; Number of blades [-]
      constant 2   1.0  ; Frequency of 2nd order servo model of pitch system [Hz]
      constant 3   0.7  ; Damping ratio 2nd order servo model of pitch system [-]
      constant 4  1  ; Max. pitch speed [deg/s]
      constant 5  1  ; Max. pitch acceleration [deg/s^2]
      constant 6  -40  ; Min. pitch angle [deg] 
      constant  7 40  ; Max. pitch angle [deg] 	  
	  constant  8 -1    ; Time for pitch runaway [s]
	  constant  9 -1    ; Time for stuck blade 1 [s]
	  constant 10 0.0   ; Angle of stuck blade 1 [deg] (if > 90 deg then blade is stuck at instantaneous angle)
	end init ;
    begin output;
      general time        ;  Time                         [s]     
      general step3 10 10 0 0.872665     ;  Pitch1 demand angle          [rad]
      general constant 0     ;  Pitch2 demand angle          [rad]
      general constant 0     ;  Pitch3 demand angle          [rad]
      general constant 0    ;  Flag for emergency pitch stop         [0=off/1=on]
    end output;           
;
    begin actions;    
      constraint bearing2 angle pitch1 ; Angle pitch1 bearing    [rad]
      ;constraint bearing2 angle pitch2 ; Angle pitch2 bearing    [rad]
      ;constraint bearing2 angle pitch3 ; Angle pitch3 bearing    [rad]
    end actions;                      
  end type2_dll;
;
end dll;
;---------------------------------------------------------------------------------------------------------------------------------------------------------------- 
;
begin output;
  filename ./res/test_2 ;
  data_format  gtsdf64 ;
  buffer 30000 ;
  deltat 0.5 ;
;
  general time;
  constraint bearing2 pitch1 2 ;
; From HAWC2 to dll.
  dll type2_dll servo_with_limits outvec 1  # Time [s] ;
  dll type2_dll servo_with_limits outvec 2  # Demanded angle [rad] ;
; From dll to HAWC2.
   dll type2_dll servo_with_limits inpvec 1  # Actual angle [rad] ;
   dll type2_dll servo_with_limits inpvec 2  # Actual speed [rad/s] ;
   dll type2_dll servo_with_limits inpvec 3  # Actual acceleration [rad^2/s] ;
end output;
;
exit;
