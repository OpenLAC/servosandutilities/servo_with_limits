---
title: Equations of motion
author: Riccardo Riva and Mads Mølgaard Pedersen
---

## Equation of motion of the harmonic oscillator

We start from a harmonic oscillator

$$
m \ddot{x} + c \dot{x} + k x = f
$$

By dividing by the mass we obtain

$$
\ddot{x} + \frac{c}{m} \dot{x} + \frac{k}{m} x = \frac{f}{m}
$$

We then introduce the frequency and damping

$$
\omega = \sqrt{k/m}
\qquad
\zeta = \frac{c}{2 \sqrt{k m}}
$$

so that

$$
k / m = \omega^2
\qquad
c / m = 2 \zeta \omega
$$

The equation of motion becomes

$$
\ddot{x} + 2 \zeta \omega \dot{x} + \omega^2 x = \frac{f}{m}
$$

## Reference displacement

To impose the reference displacement we set \(f\) to

$$
f(t) = \omega^2 x_{\mathrm{ref}}(t)
$$

so that the equation of motion becomes

$$
\ddot{x} + 2 \zeta \omega \dot{x} + \omega^2 x = \omega^2 x_{\mathrm{ref}}
$$

$$
\ddot{x} + 2 \zeta \omega \dot{x} + \omega^2 x - \omega^2 x_{\mathrm{ref}} = 0
$$

$$
\ddot{x} + 2 \zeta \omega \dot{x} + \omega^2( x - x_{\mathrm{ref}}) = 0
$$

Next, we will simulate this system.

We set \(\omega = 1\) Hz and \(\zeta = 0.2\) and set the reference displacement to a square wave with a frequency of 0.1 Hz. The position, velocity and accelerations are shown in the following figures. As we can see, the displacement follows the reference one, and oscillates with the frequency and damping that we have specified.

![unconstrained](../media/unconstrained.svg)


## Hard limit on the acceleration

We will now add a hard limit on the acceleration. Since this is non-smooth, we expect it to worsen the convergence of the ODE integrator.
The limit is imposed by clipping the acceleration to \(\pm a_{\mathrm{max}}\). So that the equation of motion becomes

$$
\ddot{x} = \min(\max(- \omega^2( x - x_{\mathrm{ref}}) - 2 \zeta \omega \dot{x} ), -a_{\mathrm{max}}), +a_{\mathrm{max}})
$$

The following plot shows that the acceleration is now within the specified limit. As expected, the displacement cannot follow the reference one as fast as before.

![hard amax](../media/hard_amax.svg)


## Soft limit on the acceleration

We will now add a soft limit on the acceleration, which is obtained by
- Scaling the acceleration by the imposed limit.
- Applying the \(\tanh\) function.
- Inverse-scaling the acceleration.

The acceleration is now computed as

$$
\ddot{x} = a_{\mathrm{max}} \tanh((- 2 \zeta \omega \dot{x} - \omega^2( x - x_{\mathrm{ref}}))/ a_{\mathrm{max}})
$$

When the acceleration is small, and the limit is large, the argument of \(\tanh\) is close to 0, and therefore will pass almost unchanged. Instead, when the acceleration exceeds the limit, the argument of \(\tanh\) will be large, and will be clipped to \(\pm 1\).

Let's start by setting a very large limit for the acceleration, \(a_{\mathrm{max}} = 1000\), and verify that the solution is the same as the unconstrained case.

![soft amax 1](../media/soft_amax_1.svg)

Now, we can set again \(a_{\mathrm{max}} = 30\) and observe how the solution has changed. In the following figure we can observe that the acceleration limit has been respected, and that the acceleration is smoother than with the har max. This has two implications:

- The ODE solver does not need as many iterations.
- The displacement follows the reference one at an even slower pace.

![soft amax 2](../media/soft_amax_2.svg)

The number of function evaluations is shown in the following table.

{!docs/table_nfev.md!}
