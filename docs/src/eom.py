# -*- coding: utf-8 -*-
"""
Equation of motion for the servo_with_limits dll.

@author: ricriv
"""

# %% Import and set up.

import os

import matplotlib

matplotlib.use("Agg")
# matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import solve_ivp
from scipy.signal import square

plt.close("all")

# Set figure properties.
matplotlib.rcdefaults()
plt.rc(
    "axes",
    labelweight="bold",
    titleweight="bold",
    labelsize=10,
    titlesize=10,
    grid=True,
)
plt.rc("legend", fontsize=10)
plt.rc("xtick", labelsize=10)
plt.rc("ytick", labelsize=10)
plt.rc("grid", linewidth=0.5)

figsize_3 = (18.0 / 2.54, 4 * 3 / 2.54)

fig_folder = "../fig"
os.makedirs(fig_folder, exist_ok=True)


# %% Unconstrained case.

# Frequency and damping of the harmonic oscillator.
omega = 1.0 * 2 * np.pi
zeta = 0.2

# Time array.
time = np.linspace(0.0, 25.0, 1001, endpoint=False)


# Reference displacement.
def x_reference(t):
    return square(2.0 * np.pi * 0.1 * t)


x_ref_all = x_reference(time)

# Initial condition.
x0 = np.array([1, 0])


# ODE.
def acc_unconstrained(pos, vel, omega, zeta, x_ref):
    return -(omega**2) * (pos - x_ref) - 2 * zeta * omega * vel


def ode(t, x, acc_fun, omega, zeta, x_ref_fun, *args):
    # Evaluate reference displacement at current time.
    x_ref = x_ref_fun(t)

    # Compute speed and acceleration.
    Dx = np.array(
        [
            x[1],
            acc_fun(x[0], x[1], omega, zeta, x_ref, *args),
        ]
    )
    return Dx


sol_unconstrained = solve_ivp(
    ode,
    (time[0], time[-1]),
    x0,
    t_eval=time,
    args=(acc_unconstrained, omega, zeta, x_reference),
)

# Compute acceleration.
acc_unconstrained = acc_unconstrained(
    sol_unconstrained.y[0, :], sol_unconstrained.y[1, :], omega, zeta, x_ref_all
)

fig_name = "unconstrained"
fig, ax = plt.subplots(num=fig_name, nrows=3, sharex=True, figsize=figsize_3)
ax[-1].set_xlabel("Time [s]")
ax[0].set_ylabel("Displacement")
ax[1].set_ylabel("Velocity")
ax[2].set_ylabel("Acceleration")
ax[0].plot(time, x_ref_all, label="Reference", color="C0")
ax[0].plot(time, sol_unconstrained.y[0, :], color="C1", label="Unconstrained")
ax[1].plot(time, sol_unconstrained.y[1, :], label="Unconstrained", color="C1")
ax[2].plot(time, acc_unconstrained, label="Unconstrained", color="C1")
ax[0].legend(bbox_to_anchor=(1.04, 1), loc="upper left")
plt.tight_layout()
fig.savefig(
    f"{fig_folder}/{fig_name}.svg",
    bbox_inches="tight",
    pad_inches=0.0,
)


# %% Hard limit on the acceleration.


def acc_hard_amax(pos, vel, omega, zeta, x_ref, amax):
    return np.clip(-(omega**2) * (pos - x_ref) - 2 * zeta * omega * vel, -amax, +amax)


# Max acceleration.
amax = 30.0

sol_hard_amax = solve_ivp(
    ode,
    (time[0], time[-1]),
    x0,
    t_eval=time,
    args=(acc_hard_amax, omega, zeta, x_reference, amax),
)

# Compute acceleration.
acc_hard_amax = acc_hard_amax(
    sol_hard_amax.y[0, :], sol_hard_amax.y[1, :], omega, zeta, x_ref_all, amax
)

fig_name = "hard_amax"
fig, ax = plt.subplots(num=fig_name, nrows=3, sharex=True, figsize=figsize_3)
ax[-1].set_xlabel("Time [s]")
ax[0].set_ylabel("Displacement")
ax[1].set_ylabel("Velocity")
ax[2].set_ylabel("Acceleration")
#
ax[0].plot(time, x_ref_all, label="Reference", color="C0")
ax[0].plot(time, sol_unconstrained.y[0, :], color="C1", label="Unconstrained")
ax[0].plot(time, sol_hard_amax.y[0, :], label="Hard limit on acceleration", color="C2")
#
ax[1].plot(time, sol_unconstrained.y[1, :], label="Unconstrained", color="C1")
ax[1].plot(time, sol_hard_amax.y[1, :], label="Hard limit on acceleration", color="C2")
#
ax[2].plot(time, acc_unconstrained, label="Unconstrained", color="C1")
ax[2].plot(time, acc_hard_amax, label="Hard limit on acceleration", color="C2")
ax[2].hlines([-amax, +amax], time[0], time[-1], colors="grey", linestyles="--")
#
ax[0].legend(bbox_to_anchor=(1.04, 1), loc="upper left")
plt.tight_layout()
fig.savefig(
    f"{fig_folder}/{fig_name}.svg",
    bbox_inches="tight",
    pad_inches=0.0,
)


# %% Soft limit on the acceleration.


def acc_soft_amax(pos, vel, omega, zeta, x_ref, amax):
    return amax * np.tanh((-(omega**2) * (pos - x_ref) - 2 * zeta * omega * vel) / amax)


# Max acceleration.
amax = 1e3

sol_soft_amax_1 = solve_ivp(
    ode,
    (time[0], time[-1]),
    x0,
    t_eval=time,
    args=(acc_soft_amax, omega, zeta, x_reference, amax),
)

# Compute acceleration.
acc_soft_amax_1 = acc_soft_amax(
    sol_soft_amax_1.y[0, :], sol_soft_amax_1.y[1, :], omega, zeta, x_ref_all, amax
)

fig_name = "soft_amax_1"
fig, ax = plt.subplots(num=fig_name, nrows=3, sharex=True, figsize=figsize_3)
ax[-1].set_xlabel("Time [s]")
ax[0].set_ylabel("Displacement")
ax[1].set_ylabel("Velocity")
ax[2].set_ylabel("Acceleration")
#
ax[0].plot(time, x_ref_all, label="Reference", color="C0")
ax[0].plot(time, sol_unconstrained.y[0, :], color="C1", label="Unconstrained")
# ax[0].plot(time, sol_hard_amax.y[0, :], label="Hard limit on acceleration", color="C2")
ax[0].plot(
    time, sol_soft_amax_1.y[0, :], label="Soft limit on acceleration", color="C3"
)
#
ax[1].plot(time, sol_unconstrained.y[1, :], label="Unconstrained", color="C1")
# ax[1].plot(time, sol_hard_amax.y[1, :], label="Hard limit on acceleration", color="C2")
ax[1].plot(
    time, sol_soft_amax_1.y[1, :], label="Soft limit on acceleration", color="C3"
)
#
ax[2].plot(time, acc_unconstrained, label="Unconstrained", color="C1")
# ax[2].plot(time, acc_hard_amax, label="Hard limit on acceleration", color="C2")
ax[2].plot(time, acc_soft_amax_1, label="Soft limit on acceleration", color="C3")
# ax[2].hlines([-amax, +amax], time[0], time[-1], colors="grey", linestyles="--")
#
ax[0].legend(bbox_to_anchor=(1.04, 1), loc="upper left")
plt.tight_layout()
fig.savefig(
    f"{fig_folder}/{fig_name}.svg",
    bbox_inches="tight",
    pad_inches=0.0,
)


# %% Soft limit on the acceleration.

# Max acceleration.
amax = 30.0

sol_soft_amax_2 = solve_ivp(
    ode,
    (time[0], time[-1]),
    x0,
    t_eval=time,
    args=(acc_soft_amax, omega, zeta, x_reference, amax),
)

# Compute acceleration.
acc_soft_amax_2 = acc_soft_amax(
    sol_soft_amax_2.y[0, :], sol_soft_amax_2.y[1, :], omega, zeta, x_ref_all, amax
)

fig_name = "soft_amax_2"
fig, ax = plt.subplots(num=fig_name, nrows=3, sharex=True, figsize=figsize_3)
ax[-1].set_xlabel("Time [s]")
ax[0].set_ylabel("Displacement")
ax[1].set_ylabel("Velocity")
ax[2].set_ylabel("Acceleration")
#
ax[0].plot(time, x_ref_all, label="Reference", color="C0")
ax[0].plot(time, sol_unconstrained.y[0, :], color="C1", label="Unconstrained")
ax[0].plot(time, sol_hard_amax.y[0, :], label="Hard limit on acceleration", color="C2")
ax[0].plot(
    time, sol_soft_amax_2.y[0, :], label="Soft limit on acceleration", color="C3"
)
#
ax[1].plot(time, sol_unconstrained.y[1, :], label="Unconstrained", color="C1")
ax[1].plot(time, sol_hard_amax.y[1, :], label="Hard limit on acceleration", color="C2")
ax[1].plot(
    time, sol_soft_amax_2.y[1, :], label="Soft limit on acceleration", color="C3"
)
#
ax[2].plot(time, acc_unconstrained, label="Unconstrained", color="C1")
ax[2].plot(time, acc_hard_amax, label="Hard limit on acceleration", color="C2")
ax[2].plot(time, acc_soft_amax_2, label="Soft limit on acceleration", color="C3")
ax[2].hlines([-amax, +amax], time[0], time[-1], colors="grey", linestyles="--")
#
ax[0].legend(bbox_to_anchor=(1.04, 1), loc="upper left")
plt.tight_layout()
fig.savefig(
    f"{fig_folder}/{fig_name}.svg",
    bbox_inches="tight",
    pad_inches=0.0,
)


# %% Collect functions evaluation.

nfev = {
    "Unconstrained": sol_unconstrained.nfev,
    "Hard amax": sol_hard_amax.nfev,
    "Soft amax": sol_soft_amax_2.nfev,
}

with open("../table_nfev.md", "w") as fid:
    fid.write(
        """
| Case | # |
| ---- | - |
"""
    )
    for key, val in nfev.items():
        fid.write(f"| {key} | {val} |\n")
