module servo_with_limit
contains
  subroutine init_servo_with_limits(array1,array2) bind(C,NAME='init_servo_with_limits')
  use servo_with_limits_data
  use mod_angles, only: deg2rad, two_pi
  use BuildInfo
  implicit none
!DEC$ ATTRIBUTES DLLEXPORT ::init_servo_with_limits
  real*8, intent(in) :: array1(100)
    !! Input array, from HAWC2 to dll. It must contain:
    !!
    !! -  1: Number of blades [-]
    !! -  2: Frequency of 2nd order servo model of pitch system [Hz]
    !! -  3: Damping ratio 2nd order servo model of pitch system [-]
    !! -  4: Maximum pitch speed [deg/s]
    !! -  5: Maximum pitch acceleration [deg/s**2]
    !! -  6: Minimum pitch angle [deg]
    !! -  7: Maximum pitch angle [deg]
    !! -  8: Time for pitch runaway [s]
    !! -  9: Time for blade 1 stuck [s]
    !! - 10: Angle of blade 1 stuck [s]
    !! - 11: 1 to unwrap the demanded angle before integrating it. 0 to pass it as it is. The default is 0. This feature is recommended whenever the demanded angle is expected to exceed 360 deg, such as when this dll is used to model a yaw bearing.
  real*8, intent(out) :: array2(1)
    !! Output array, from dll to HAWC2. 0 is returned for the first element.
  type(tbuildinfo)  :: binfo
!
  !call BuildInfo_echo(binfo)
  write(6,*) 'Pitch Servo ' //trim(adjustl(binfo%git_tag))// ' loaded...'
! Save parameters
  nblades      = int(array1(1))
  omega0       =     array1(2) * two_pi
  beta0        =     array1(3)
  vmax         =     array1(4) * deg2rad
  amax         =     array1(5) * deg2rad
  theta_min    =     array1(6) * deg2rad
  theta_max    =     array1(7) * deg2rad
  time_runaway =     array1(8)
  time_stuck   =     array1(9)
  if (abs(array1(10)).lt.90.d0) then 
    stuck_type=0
    stuck_angle=array1(10) * deg2rad
  else
    stuck_type=1
  endif
  flag_unwrap = array1(11) > 0.d0
! Set initial conditions
  allocate(yold(2, nblades),  &
           ynew(2, nblades),  &
           source=0.d0)
! Set oldtime
  stepno=0
  oldtime=0.d0
  array2(1) = 0.d0
  end subroutine init_servo_with_limits
!***********************************************************************
  subroutine update_servo_with_limits(array1,array2) bind(C,NAME='update_servo_with_limits')
  use servo_with_limits_data
  use mod_angles, only: f_unwrap

  implicit none
!DEC$ ATTRIBUTES DLLEXPORT ::update_servo_with_limits

  real*8, intent(in) :: array1(*)
    !! Input array, from HAWC2 to dll. It must contain:
    !!
    !!  - 1: Time                                  [s]
    !!  - 2: Pitch1 demand angle                   [rad]
    !!  - 3: Pitch2 demand angle                   [rad]
    !!  - 4: Pitch3 demand angle                   [rad]
    !!  - 5: Emergency stop flag                   [0/1]
  real*8, intent(out) :: array2(*)
    !! Output array, from dll to HAWC2. It contains:
    !!
    !!  -           1: Pitch1 angle                          [rad]
    !!  -           2: Pitch2 angle                          [rad]
    !!  -           3: Pitch3 angle                          [rad]
    !!  -   nblades+1: Pitch1 velocity                       [rad/s]
    !!  -   nblades+2: Pitch2 velocity                       [rad/s]
    !!  -   nblades+3: Pitch3 velocity                       [rad/s]
    !!  - 2*nblades+1: Pitch1 acceleration                   [rad/s^2]
    !!  - 2*nblades+2: Pitch2 acceleration                   [rad/s^2]
    !!  - 2*nblades+3: Pitch3 acceleration                   [rad/s^2]
!
! Local variables
!  integer*4 i,j,ido
  integer*4 i,j
  !real*8 tol,param(50),y(2),t,tend,timestep
  real*8 tol,y(2),t,tend
  !parameter(tol=1.d-5)
  real*8 theta
  real*8 work(15),relerr,abserr
  parameter(abserr=1.d-8)
  integer*4 iflag,iwork(5)
  logical emergency_stop
  external rkf45
! Initial call values
  relerr=1.d-6
  iflag=1
! Check if the time has changed
  if (array1(1)-oldtime.gt.0.d0) then
    timestep=array1(1)-oldtime
    ! The values from the last time step are now old.
    oldtime=array1(1)
    yold=ynew
    stepno=stepno+1
  endif
  ! Set emergency stop.
  ! Use the Python convention that any number greater than 0 is True.
  emergency_stop = array1(5) > 0.d0
  if (stepno == 1) emergency_stop = .false.
! Loop for all blades
  do i=1,nblades
!   Initial conditions for pitch angle and velocity
    t=0.d0
    y(1:2)=yold(1:2,i)
    ! Take pitch demand angle for blade i. Note that time is the first element in array1.
    theta_ref = array1(i+1)
    ! Unwrap the reference angle before feeding it to the Runge Kutta integrator.
    if (flag_unwrap) theta_ref = f_unwrap(yold(1, i), theta_ref)
!   Compute pitch angle and velocity at next step 
    tend=timestep
    if ((array1(1).gt.time_stuck).and.(time_stuck.gt.0.d0).and..not.emergency_stop.and.(i.eq.1)) then
      if (stuck_type.eq.1) then
        stuck_angle=yold(1,1)
      endif
      y(1)=stuck_angle
      y(2)=0.d0
    elseif ((array1(1).gt.time_runaway).and.(time_runaway.gt.0.d0).and..not.emergency_stop) then
      y(1)=y(1)+y(2)*timestep
      y(2)=max(-vmax,y(2)-amax)
    elseif (emergency_stop) then
      y(1)=y(1)+y(2)*timestep
      y(2)=min(vmax,y(2)+amax)
    else
      call rkf45(ode,2,y,t,tend,relerr,abserr,iflag,work,iwork)
    endif
!   Apply hard limits on angles
    if (y(1).lt.theta_min) then
      y(1)=theta_min
      y(2)=0.d0
    endif
    if (y(1).gt.theta_max) then
      y(1)=theta_max
      y(2)=0.d0
    endif
!   Save results
    ynew(1:2,i)=y(1:2)
!   Fill output array2
    oldarray2(i)=y(1)
    oldarray2(nblades+i)=y(2)
    oldarray2(2*nblades+i)=(y(2)-yold(2,i))/timestep
  enddo
! Insert output
  array2(1:3*nblades)=oldarray2(1:3*nblades)
  end subroutine update_servo_with_limits
!***********************************************************************
  subroutine ode(t,y,yprime)
  use servo_with_limits_data
  implicit none
  real*8, intent(in) :: t
    !! Time [s]
  real*8, intent(in) :: y(2)
    !! State: angle and angular speed [rad, rad/s]
  real*8, intent(out) :: yprime(2)
    !! Derivative of the state: angular speed and angular acceleration [rad/s, rad/s**2]
  !
! ODEs
  yprime(1) = y(2)
  yprime(2) = -amax/vmax*y(2)&
              +amax*tanh(omega0**2*(theta_ref-y(1))/amax-&
                         (2.d0*beta0*omega0-amax/vmax)*y(2)/amax)
  end subroutine ode
!***********************************************************************
end module servo_with_limit
